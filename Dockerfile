FROM bitnami/php-fpm:7.4-prod

RUN useradd -u 1001 -r -g 0 -d /app -s /bin/bash -c "Default Application User" default \
    && mkdir -p /app \
    && chown -R 1001:0 /app && chmod -R g+rwX /app

USER 1001

WORKDIR /app

ADD . ./

RUN composer install
